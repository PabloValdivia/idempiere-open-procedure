#! /bin/sh
### auto upgrade script by Topgiga IT Ray
declare -i ver_s
declare -i ver_l

wget -O version_s http://192.168.22.93/mrp/version 
if [ -e version_s ]
then
    ver_s=$(cat version_s)
else
    ver_s=0
fi	

echo $ver_s

if [ -e version ]
then
    ver_l=$(cat version)
else
    ver_l=0
fi
echo $ver_l
if [ $ver_s != $ver_l ]
then
   if [ -e last ]
   then
      # nothing
      rm -f last/*
   else	   
      mkdir last	
   fi
   cp -f version last/

   echo "Downloading"
   mv -f topgiga_workshops-0.0.1-SNAPSHOT.jar last/
   wget http://192.168.22.93/mrp/topgiga_workshops-0.0.1-SNAPSHOT.jar
   if [ -e topgiga_workshops-0.0.1-SNAPSHOT.jar ]
   then
       mv version_s version

   fi
   
   echo "done"
else
   echo "run"

fi
find ~/mrp/log -mtime +10 -type f -delete
java -cp /home/pi/topgiga_workshops-0.0.1-SNAPSHOT.jar tw.topgiga.mrpclinet.MRPClinet
#echo $ver_s 
#echo $ver_l
