CREATE OR REPLACE FUNCTION nn_defaultprice_sp(pinstance numeric)
  RETURNS void AS
$BODY$
DECLARE

ResultStr VARCHAR (200);
roleaccesslevelwin VARCHAR (200);
sql text;

p RECORD;
r RECORD;
s RECORD;
inv RECORD;

p_Record_ID NUMERIC(10) := 0;
p_AD_Client_ID NUMERIC(10) := 0;
p_AD_Org_ID NUMERIC(10) := 0;
p_AD_User_ID NUMERIC(10) := 0;
p_User_ID NUMERIC(10) := 0;
p_C_BPartner_ID NUMERIC(10) := 0;
p_M_Product_ID NUMERIC(10) := 0;
p_M_Product_ID_TO NUMERIC(10) := 0;
p_M_Product_Category_ID NUMERIC(10) := 0;
p_C_DocType_ID NUMERIC(10) := 0;
p_C_BPartner_ID_TO NUMERIC(10) := 0;
p_C_DocType_ID_TO NUMERIC(10) := 0;

p_C_BPartner_Value VARCHAR (40) := 0;
p_C_DocType_Value VARCHAR (40) := 0;
p_C_BPartner_Value_TO VARCHAR (40) := '';
p_C_DocType_Value_TO VARCHAR (40) := '';

p_MovementDate DATE;
p_MovementDate_TO DATE;
v_message VARCHAR (400) := '';
v_NextNo NUMERIC(10) := 0;



BEGIN

IF pinstance is null THEN pinstance:=0;
END IF;

BEGIN
ResultStr := 'PInstanceNotFound';
UPDATE adempiere.ad_pinstance
SET created = SYSDATE,
isprocessing = 'Y',
reslut = 0
WHERE ad_pinstance_id = pinstance_id;
COMMIT;
EXCEPTION
WHEN OTHERS THEN NULL;

END;

FOR p IN
  (SELECT pi.record_id,
          pi.ad_client_id,
          pi.ad_org_id,
          pi.ad_user_id,
          pp.parametername,
          pp.p_string,
          pp.p_number,
          pp.p_date,
          pp.p_string_TO,
          pp.p_number_TO,
          pp.p_date_tO
   FROM adempiere.ad_pinstance pi
   LEFT OUTER JOIN adempiere.ad_pinstance_para pp ON(pi.ad_pinstance_id = pp.ad_pinstance_id)
   WHERE pi.ad_pinstance_id = pinstance
   ORDER BY pp.SeqNo) 
LOOP 
  p_Record_ID := p.record_id;
  p_User_ID := p.AD_User_id;
  p_AD_Client_ID := p.AD_Client_ID;
  p_AD_Org_ID := p.AD_Org_ID;

  IF p.parametername = 'AD_Client_ID' THEN p_AD_Client_ID = p.p_number;
  ELSIF p.parametername = 'M_Product_Category_ID' THEN p_M_Product_Category_ID = p.p_number;
  ELSIF p.parametername = 'AD_User_ID' THEN p_AD_User_ID = p.p_number;
  END IF;

 END LOOP;

IF p_User_ID IS NULL THEN 
   p_User_ID := 0; 
END IF;

IF pinstance = 0 THEN
p_MovementDate := date_trunc('YEAR', now());
p_MovementDate_TO:= date_trunc('DAY', now());
END IF;


v_message :='GET Deault Price List';

FOR r IN (
select m_pricelist_version_id from m_pricelist_version where m_pricelist_id in 
(
   select m_pricelist_id from m_pricelist where ad_client_id = p_AD_Client_ID
)
  
)LOOP

v_message :='--取出所有未設單價的產碞產品';

	FOR s IN (
	SELECT m_product_id,*
		FROM m_product xx
		WHERE ad_client_id = p_AD_Client_ID and isactive = 'Y'
		AND (p_M_Product_Category_ID = 0 OR p_M_Product_Category_ID = xx.M_Product_Category_ID)
		AND not exists (
			SELECT 1 FROM m_productprice yy
			WHERE 1 = 1 
			and m_pricelist_version_id = r.m_pricelist_version_id
			AND xx.m_product_id  = yy.m_product_id
		)
  
	)LOOP

	v_message :='v_NextNo := v_NextNo + 1;';
	v_NextNo := v_NextNo + 1;
	v_message :='before insert';

INSERT INTO m_productprice(
            m_pricelist_version_id, m_product_id, ad_client_id, ad_org_id, 
            isactive, created, createdby, updated, updatedby, pricelist, 
            pricestd, pricelimit, m_productprice_uu, m_productprice_id)
    VALUES (r.m_pricelist_version_id, s.m_product_id, p_AD_Client_ID, 0, 
            'Y', now(), 100, now(), 100, 0, 
            0, 0, uuid_generate_v4(), nextidbytablename('M_ProductPrice','Y'));

	END LOOP;
END LOOP;

v_message :='DONE';
v_message :=  concat('新增了',  v_NextNo, ' 筆資料');
IF pinstance > 0 THEN
BEGIN
UPDATE adempiere.ad_pinstance
SET updated = now(),
isprocessing = 'N',
result = 1,
errormsg = v_message
WHERE ad_pinstance_id = pinstance;
-- COMMIT;
EXCEPTION
WHEN OTHERS THEN NULL;
END;
END IF;


EXCEPTION

WHEN OTHERS THEN

UPDATE adempiere.ad_pinstance
SET updated = now(),
isprocessing = 'N',
result = 0,
errormsg = v_message
WHERE ad_pinstance_id = pinstance;
-- COMMIT;

END; 

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION nn_defaultprice_sp(numeric)
  OWNER TO adempiere;
