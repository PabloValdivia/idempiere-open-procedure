﻿-- Function: mrp_productionlog_rpt(numeric)

-- DROP FUNCTION mrp_productionlog_rpt(numeric);

CREATE OR REPLACE FUNCTION mrp_productionlog_rpt(pinstance numeric)
  RETURNS void AS
$BODY$
DECLARE
/**
出勤異常查詢
*/
ResultStr VARCHAR (200);
roleaccesslevelwin VARCHAR (200);
sql VARCHAR (2000);


p RECORD;
r RECORD;

--p_C_Invoice_ID NUMERIC(10) := 0;
p_DateAcct DATE;
p_DateAcct_TO DATE;
p_DateStep DATE;
p_DateFrom DATE;

p_Record_ID NUMERIC(10) := 0;
p_AD_User_ID NUMERIC(10) := 0;
p_User_ID NUMERIC(10) := 0;
p_AD_Client_ID NUMERIC(10) := 0;
p_AD_Org_ID NUMERIC(10) := 0;
p_AD_Org_ID_TO NUMERIC(10) := 0;
p_IsSOTrx VARCHAR (1) := 'Y';
p_WorkshopCode VARCHAR (10);
p_IsAll VARCHAR (1) := 'N';
p_AccountValue VARCHAR (10);
p_AccountValue_TO VARCHAR (10);
p_Workshop_ID NUMERIC(10);
p_DocumentNo VARCHAR (30);
p_LOT VARCHAR (40);
p_MovementDate DATE;
p_MovementDate_TO DATE;
p_DateInvoiced DATE;
p_DateInvoiced_TO DATE;
p_DatePromised DATE;
p_DatePromised_TO DATE;

p_Product_ID NUMERIC(10) ;
v_message VARCHAR (400) := '';
v_description VARCHAR (400) := '';
v_production_id NUMERIC(10) := 0;
v_end_product_id NUMERIC(10) := 0;
v_end_attributesetinstance_id NUMERIC(10) := 0;
v_DocumentNo VARCHAR (30);
BEGIN

IF pinstance is null THEN 
	pinstance:=0;
END IF;

v_message :='程式開始:: 更新[呼叫程序]紀錄檔...開始執行時間(created)..程序執行中(isprocessing)..';
--IF pinstance > 0 THEN
BEGIN
ResultStr := 'PInstanceNotFound';
UPDATE adempiere.ad_pinstance
SET created = SYSDATE,
isprocessing = 'Y',
reslut = 0
WHERE ad_pinstance_id = pinstance_id;
COMMIT;
EXCEPTION
WHEN OTHERS THEN NULL;

END;

v_message :='OPEN ITEM Detail';

FOR p IN
(SELECT pi.record_id, pi.ad_client_id,pi.ad_org_id, pi.ad_user_id, 
pp.parametername,
pp.p_string, pp.p_number, pp.p_date,
pp.p_string_TO, pp.p_number_TO, pp.p_date_tO
FROM adempiere.ad_pinstance pi
LEFT OUTER JOIN adempiere.ad_pinstance_para pp ON(pi.ad_pinstance_id = pp.ad_pinstance_id)
WHERE pi.ad_pinstance_id = pinstance
ORDER BY pp.SeqNo)
LOOP
p_Record_ID := p.record_id;
p_User_ID := p.AD_User_id;
p_AD_Client_ID := p.AD_Client_ID;
p_AD_Org_ID := p.AD_Org_ID;

  IF p.parametername = 'DatePromised' THEN p_DatePromised = p.p_date;
     p_DatePromised_TO = p.p_date_to;
  ELSIF p.parametername = 'M_Workshop_ID' THEN p_Workshop_ID = p.p_number;
  ELSIF p.parametername = 'END_Product_ID' THEN p_Product_ID = p.p_number;
  ELSIF p.parametername = 'DocumentNo' THEN p_DocumentNo = p.p_string;
  ELSIF p.parametername = 'LOT' THEN p_LOT = p.p_string;



  END IF;

END LOOP;


IF p_User_ID IS NULL THEN 
   p_User_ID := 0; 
END IF;

IF pinstance = 1000000 THEN

p_DateAcct := '2018-01-01';
p_DateAcct_TO := '2018-01-31';
---p_AD_User_ID := 1000784;
--p_Workshop_ID := 1000019;
--p_DocumentNo = 'MON190550';
 --p_Product_ID=1002358;
   p_LOT := 'B1907042XE';
END IF;

v_message :='Start Process';
--EXECUTE sqldel;
TRUNCATE t_workshopprocess;
/**

    select * from ad_pinstance_para where ad_pinstance_id in (select ad_pinstance_id from  ad_pinstance where  AD_Process_ID=1000315 order by ad_pinstance_id desc) order by ad_pinstance_id desc

    select value from m_product where m_product_id = 1002358

    select * from M_Productionline where isendproduct = 'Y' 
**/
-- select errormsg, * from  ad_pinstance where  AD_Process_ID=1000315 order by ad_pinstance_id desc

FOR r IN (
-- select  ad_pinstance_id, errormsg from  adempiere.ad_pinstance where ad_pinstance_id = 1000000
-- select mrp_productionlog_rpt(1000000)
-- select * from t_workshopprocess
-- drop table t_workshopprocess
-- select * from m_attributesetinstance
-- create table t_workshopprocess as 
   select 1000000 ::numeric(10,0) ad_pinstance_id 
    , 1000000 ::numeric(10,0)end_product_id 
    , 1000000 ::numeric(10,0)end_attributesetinstance_id
    , ''::VARCHAR (30) DocumentNo  
   ,* from m_workshopprocess  
   WHERE 1=1
   AND (p_Product_ID is null OR ( M_Production_ID in (select distinct M_Production_ID from M_Productionline where isendproduct = 'Y' AND  M_Product_ID = p_Product_ID)))
   AND ( p_LOT is null OR ( M_Production_ID in (select distinct M_Production_ID from M_Productionline where isendproduct = 'Y' 
				AND  m_attributesetinstance_id in ( select m_attributesetinstance_id from m_attributesetinstance where lot = p_LOT ))))
   AND  ( p_DocumentNo is null OR M_Production_ID in (select M_Production_ID from M_Production where documentno = p_DocumentNo))
   AND (p_Workshop_ID is null OR m_workshop_id = p_Workshop_ID) 
   AND (p_DatePromised is null OR  M_Production_ID in (select M_Production_ID from M_Production where DatePromised between p_DatePromised and p_DatePromised_TO) )
   
   order by m_production_id , m_workshopprocess_id
)LOOP
    v_message :='LOOP';	
    if v_production_id != r.m_production_id then
	v_message :='Variable....';

	select documentno into v_DocumentNo from m_production where m_production_id = r.m_production_id;

        select m_product_id into v_end_product_id 
	from m_productionline 
	where m_production_id = r.m_production_id and isendproduct = 'Y'
	order by line limit 1;

	        
        select m_attributesetinstance_id into v_end_attributesetinstance_id 
	from m_productionline 
	where m_production_id = r.m_production_id and isendproduct = 'Y'
	order by line limit 1;
        v_production_id := r.m_production_id;

        
        
    end if;


INSERT INTO t_workshopprocess(

            ad_pinstance_id, end_product_id, end_attributesetinstance_id, 
            ad_client_id, ad_org_id, created, createdby, description, isactive, 
            m_productionnode_id, m_workshop_id, m_workshopprocess_id, m_workshopprocess_uu, 
            name, seqno, updated, updatedby, value, m_productionnodeprocess_id, 
            processed, m_product_id, plannedqty, qtyused, m_production_id, 
            ad_user_id, approved_user_id, refname, refvalue, recordname, 
            ad_wf_activity_id, recordvalue, m_productionline_id, nodeprocessed, 
            starttime, m_attributesetinstance_id, isvalid , documentno)
            
    VALUES (pinstance, v_end_product_id, v_end_attributesetinstance_id, 
            r.ad_client_id, r.ad_org_id, r.created, r.createdby, r.description, r.isactive, 
            r.m_productionnode_id, r.m_workshop_id, r.m_workshopprocess_id, r.m_workshopprocess_uu, 
            r.name, r.seqno, r.updated, r.updatedby, r.value, r.m_productionnodeprocess_id, 
            r.processed, r.m_product_id, r.plannedqty, r.qtyused, r.m_production_id, 
            r.ad_user_id, r.approved_user_id, r.refname, r.refvalue, r.recordname, 
            r.ad_wf_activity_id, r.recordvalue, r.m_productionline_id, r.nodeprocessed, 
            r.starttime, r.m_attributesetinstance_id, r.isvalid ,v_DocumentNo);

	
END LOOP;

v_message :='1END Process';

IF pinstance > 0 THEN
BEGIN
UPDATE adempiere.ad_pinstance
SET updated = now(),
isprocessing = 'N',
result = 1,
errormsg = v_message
WHERE ad_pinstance_id = pinstance;
-- COMMIT;
EXCEPTION
WHEN OTHERS THEN NULL;
END;
END IF;

EXCEPTION

WHEN OTHERS THEN
--sqlins := 'INSERT INTO adempiere.t_an_shipment (ad_pinstance_id, t_an_shipment_id, bp_name) VALUES($1, $2, $3)';
--EXECUTE sqlins USING pinstance, 9, v_message;
--v_message :='例外錯誤。。。';
UPDATE adempiere.ad_pinstance
SET updated = now(),
isprocessing = 'N',
result = 0,
errormsg = v_message
WHERE ad_pinstance_id = pinstance;
-- COMMIT;

END; 

$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION mrp_productionlog_rpt(numeric)
  OWNER TO adempiere;
